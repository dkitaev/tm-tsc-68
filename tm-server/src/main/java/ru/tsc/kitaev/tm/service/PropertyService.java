package ru.tsc.kitaev.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['session.secret']}")
    private String sessionSecret;

    @Value("#{environment['session.iteration']}")
    private Integer sessionIteration;

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['developer.name']}")
    private String developerName;

    @Value("#{environment['developer.email']}")
    private String developerEmail;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @Value("#{environment['jdbc.user']}")
    private String jdbcUser;

    @Value("#{environment['jdbc.password']}")
    private String jdbcPassword;

    @Value("#{environment['jdbc.url']}")
    private String jdbcUrl;

    @Value("#{environment['jdbc.driver']}")
    private String jdbcDriver;

    @Value("#{environment['jdbc.sql_dialect']}")
    private String jdbcSqlDialect;

    @Value("#{environment['jdbc.hbm2ddl_auto']}")
    private String jdbcNbm2ddlAuto;

    @Value("#{environment['jdbc.show_sql']}")
    private String jdbcShowSql;

    @Value("#{environment['hibernate.cache.use_second_level_cache']}")
    private String hibernateCacheUseSecondLevelCache;

    @Value("#{environment['hibernate.cache.provider_configuration_file_resource_path']}")
    private String hibernateCacheProviderConfigurationFileResourcePath;

    @Value("#{environment['hibernate.cache.region.factory_class']}")
    private String hibernateCacheRegionFactoryClass;

    @Value("#{environment['hibernate.cache.use_query_cache']}")
    private String hibernateCacheUseQueryCache;

    @Value("#{environment['hibernate.cache.use_minimal_puts']}")
    private String hibernateCacheUseMinimalPuts;

    @Value("#{environment['hibernate.cache.hazelcast.use_lite_member']}")
    private String hibernateCacheHazelcastUseLiteMember;

    @Value("#{environment['hibernate.cache.region_prefix']}")
    private String hibernateCacheRegionPrefix;

}
