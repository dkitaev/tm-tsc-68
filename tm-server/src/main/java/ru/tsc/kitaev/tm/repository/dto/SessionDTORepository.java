package ru.tsc.kitaev.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.dto.SessionDTO;

@Repository
public interface SessionDTORepository extends AbstractDTORepository<SessionDTO> {

}
