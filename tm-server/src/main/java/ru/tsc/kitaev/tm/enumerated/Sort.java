package ru.tsc.kitaev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.comparator.ComparatorByFinishDate;
import ru.tsc.kitaev.tm.comparator.ComparatorByName;
import ru.tsc.kitaev.tm.comparator.ComparatorByStartDate;
import ru.tsc.kitaev.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    FINISH_DATE("Sort by date finish", ComparatorByFinishDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}
