<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK EDIT</h1>
<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">

    <form:input type="hidden" path="id"/>

    <p>
    <div style="font-weight: bold">NAME:</div>
    <div><form:input type="text" path="name"/></div>
    </p>


    <p>
    <div style="font-weight: bold">DESCRIPTION:</div>
    <div><form:input type="text" path="description"/></div>
    </p>

    <p>
    <div style="font-weight: bold">PROJECT:</div>
    <div>
        <form:select path="projectId">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </div>
    </p>

    <p>
    <div style="font-weight: bold">STATUS:</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    </p>

    <p>
    <div style="font-weight: bold">CREATED:</div>
    <div>
        <form:input type="date" path="startDate"/>
    </div>
    </p>

    <p>
    <div style="font-weight: bold">DATE FINISH:</div>
    <div>
        <form:input type="date" path="finishDate"/>
    </div>
    </p>

    <button type="submit">SAVE TASK</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>