package ru.tsc.kitaev.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.kitaev.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@EnableTransactionManagement
@ComponentScan("ru.tsc.kitaev.tm")
@EnableJpaRepositories("ru.tsc.kitaev.tm.repository")
public class DataBaseConfiguration {

    @Autowired
    @NotNull
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getJdbcDriver());
        dataSource.setUrl(propertyService.getJdbcUrl());
        dataSource.setUsername(propertyService.getJdbcUser());
        dataSource.setPassword(propertyService.getJdbcPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.kitaev.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getJdbcSqlDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getJdbcNbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getJdbcShowSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getHibernateCacheUseSecondLevelCache());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getHibernateCacheUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getHibernateCacheUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getHibernateCacheRegionPrefix());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getHibernateCacheRegionFactoryClass());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getHibernateCacheProviderConfigurationFileResourcePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
