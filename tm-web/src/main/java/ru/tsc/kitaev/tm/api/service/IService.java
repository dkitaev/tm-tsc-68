package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    @NotNull
    E add(@NotNull final E entity);

    @NotNull
    E update(@NotNull final E entity);

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull final String id);

    boolean existsById(@Nullable final String id);

    int getSize();

    void clear();

    void delete(@NotNull final E entity);

    void deleteById(@NotNull final String id);

}
