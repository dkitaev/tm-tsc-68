package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.client.TaskRestEndpointClient;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskRestEndpointClient {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    @NotNull
    @PostMapping("/add")
    public Task add(@NotNull @RequestBody final Task task) {
        return taskService.add(task);
    }

    @Override
    @NotNull
    @PutMapping("/save")
    public Task save(@NotNull @RequestBody final Task task) {
        return taskService.update(task);
    }

    @Override
    @Nullable
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @PathVariable(value = "id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@Nullable @PathVariable(value = "id") final String id) {
        return taskService.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public int getSize() {
        return taskService.getSize();
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@NotNull @RequestBody final Task task) {
        taskService.delete(task);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable(value = "id") final String id) {
        taskService.deleteById(id);
    }

    @Override
    @GetMapping("/findAllByProjectId/{projectId}")
    public List<Task> findAllByProjectId(@Nullable @PathVariable(value = "projectId") final String projectId) {
        return taskService.findAllByProjectId(projectId);
    }

}
