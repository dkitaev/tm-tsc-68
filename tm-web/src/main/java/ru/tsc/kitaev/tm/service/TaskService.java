package ru.tsc.kitaev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.repository.TaskRepository;

import java.util.List;

@Service
@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    @Nullable
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return repository.findAllByProjectId(projectId);
    }

}
