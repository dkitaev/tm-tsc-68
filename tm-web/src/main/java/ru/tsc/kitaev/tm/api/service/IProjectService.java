package ru.tsc.kitaev.tm.api.service;

import ru.tsc.kitaev.tm.model.Project;

public interface IProjectService extends IService<Project> {

}
