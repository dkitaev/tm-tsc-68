package ru.tsc.kitaev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.soap.*;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://kitaev.tsc.ru/tm/soap";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskAddRequest", namespace = NAMESPACE)
    public TaskAddResponse taskAdd(@RequestPayload final TaskAddRequest request) {
        return new TaskAddResponse(taskService.add(request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse taskSave(@RequestPayload final TaskSaveRequest request) {
        return new TaskSaveResponse(taskService.update(request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse taskFindAll(@RequestPayload final TaskFindAllRequest request) {
        return new TaskFindAllResponse(taskService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse taskFindById(@RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(taskService.findById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse taskExistsById(@RequestPayload final TaskExistsByIdRequest request) {
        return new TaskExistsByIdResponse(taskService.existsById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskGetSizeRequest", namespace = NAMESPACE)
    public TaskGetSizeResponse taskGetSize(@RequestPayload final TaskGetSizeRequest request) {
        return new TaskGetSizeResponse(taskService.getSize());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse taskClear(@RequestPayload final TaskClearRequest request) {
        taskService.clear();
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse taskDelete(@RequestPayload final TaskDeleteRequest request) {
        taskService.delete(request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse taskDeleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = NAMESPACE)
    public TaskFindAllByProjectIdResponse taskFindAllByProjectId(@RequestPayload final TaskFindAllByProjectIdRequest request) {
        return new TaskFindAllByProjectIdResponse(taskService.findAllByProjectId(request.getProjectId()));
    }

}
