package ru.tsc.kitaev.tm.soap;

import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public Project createProject() {
        return new Project();
    }

    public ProjectAddRequest createProjectAddRequest() {
        return new ProjectAddRequest();
    }

    public ProjectAddResponse createProjectAddResponse() {
        return new ProjectAddResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectExistsByIdRequest createProjectExistsByIdRequest() {
        return new ProjectExistsByIdRequest();
    }

    public ProjectExistsByIdResponse createProjectExistsByIdResponse() {
        return new ProjectExistsByIdResponse();
    }

    public ProjectGetSizeRequest createProjectGetSizeRequest() {
        return new ProjectGetSizeRequest();
    }

    public ProjectGetSizeResponse createProjectGetSizeResponse() {
        return new ProjectGetSizeResponse();
    }

    public ProjectClearRequest createProjectClearRequest() {
        return new ProjectClearRequest();
    }

    public ProjectClearResponse createProjectClearResponse() {
        return new ProjectClearResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public Task createTask() {
        return new Task();
    }

    public TaskAddRequest createTaskAddRequest() {
        return new TaskAddRequest();
    }

    public TaskAddResponse createTaskAddResponse() {
        return new TaskAddResponse();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskExistsByIdRequest createTaskExistsByIdRequest() {
        return new TaskExistsByIdRequest();
    }

    public TaskExistsByIdResponse createTaskExistsByIdResponse() {
        return new TaskExistsByIdResponse();
    }

    public TaskGetSizeRequest createTaskGetSizeRequest() {
        return new TaskGetSizeRequest();
    }

    public TaskGetSizeResponse createTaskGetSizeResponse() {
        return new TaskGetSizeResponse();
    }

    public TaskClearRequest createTaskClearRequest() {
        return new TaskClearRequest();
    }

    public TaskClearResponse createTaskClearResponse() {
        return new TaskClearResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskFindAllByProjectIdRequest createTaskFindAllByProjectIdRequest() {
        return new TaskFindAllByProjectIdRequest();
    }

    public TaskFindAllByProjectIdResponse createTaskFindAllByProjectIdResponse() {
        return new TaskFindAllByProjectIdResponse();
    }

}
