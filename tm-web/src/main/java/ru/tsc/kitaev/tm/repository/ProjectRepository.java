package ru.tsc.kitaev.tm.repository;

import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.Project;

@Repository
public interface ProjectRepository extends AbstractRepository<Project> {

}
