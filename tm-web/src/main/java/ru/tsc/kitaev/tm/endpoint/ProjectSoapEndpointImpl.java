package ru.tsc.kitaev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.soap.*;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://kitaev.tsc.ru/tm/soap";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = NAMESPACE)
    public ProjectAddResponse Add(@RequestPayload final ProjectAddRequest request) {
        return new ProjectAddResponse(projectService.add(request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse projectSave(@RequestPayload final ProjectSaveRequest request) {
        return new ProjectSaveResponse(projectService.update(request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse projectFindAll(@RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse projectFindById(@RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectService.findById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse projectExistsById(@RequestPayload final ProjectExistsByIdRequest request) {
        return new ProjectExistsByIdResponse(projectService.existsById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectGetSizeRequest", namespace = NAMESPACE)
    public ProjectGetSizeResponse projectGetSize(@RequestPayload final ProjectGetSizeRequest request) {
        return new ProjectGetSizeResponse(projectService.getSize());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse projectClear(@RequestPayload final ProjectClearRequest request) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse projectDelete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.delete(request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse projectDeleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

}
