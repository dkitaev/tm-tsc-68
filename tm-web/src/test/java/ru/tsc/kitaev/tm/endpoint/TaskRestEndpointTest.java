package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.client.ProjectRestEndpointClient;
import ru.tsc.kitaev.tm.client.TaskRestEndpointClient;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.marker.IntegrationCategory;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public class TaskRestEndpointTest {

    @NotNull
    private final ProjectRestEndpointClient projectClient = ProjectRestEndpointClient.client();

    @NotNull
    private final TaskRestEndpointClient taskClient = TaskRestEndpointClient.client();

    @NotNull
    private final Project project1 = new Project("Test Project 1", "Test Project Description 1");

    @NotNull
    private final Task task1 = new Task("Test Task 1", "Test Task Description 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2", "Test Task Description 2");

    @NotNull
    private final Task task3 = new Task("Test Task 3", "Test Task Description 3");

    private int count = 0;

    @Before
    public void before() {
        projectClient.add(project1);
        task1.setProjectId(project1.getId());
        taskClient.add(task1);
        taskClient.add(task2);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addByTaskTest() {
        @Nullable Task task = taskClient.add(task3);
        Assert.assertNotNull(task);
        Assert.assertEquals(task3.getName(), task.getName());
        Assert.assertEquals(task3.getDescription(), task.getDescription());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveByTaskTest() {
        @Nullable Task task = taskClient.findById(task1.getId());
        task.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskClient.save(task));
        @Nullable Task newTask = taskClient.findById(task1.getId());
        Assert.assertEquals(task.getStatus(), newTask.getStatus());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllByTasksTest() {
        @Nullable final List<Task> tasks = taskClient.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(count + 2, tasks.size());
        for (@NotNull Task task : tasks) {
            Assert.assertNotNull(taskClient.findById(task.getId()));
        }
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdByTaskTest() {
        @Nullable final Task task = taskClient.findById(task1.getId());
        Assert.assertEquals("Test Task 1", task.getName());
        Assert.assertEquals("Test Task Description 1", task.getDescription());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByIdByTaskTest() {
        Assert.assertTrue(taskClient.existsById(task1.getId()));
        Assert.assertFalse(taskClient.existsById(task3.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void getSizeTask() {
        Assert.assertEquals(count + 2, taskClient.getSize());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clearByTask() {
        Assert.assertEquals(count + 2, taskClient.getSize());
        taskClient.clear();
        Assert.assertEquals(count, taskClient.getSize());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByTaskTest() {
        taskClient.delete(task1);
        Assert.assertNull(taskClient.findById(task1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByTaskIdTest() {
        taskClient.deleteById(task1.getId());
        Assert.assertNull(taskClient.findById(task1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTaskByProjectIdTest() {
        @Nullable final List<Task> tasks = taskClient.findAllByProjectId(task1.getProjectId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(count + 1, tasks.size());
        for (@NotNull Task task : tasks) {
            Assert.assertNotNull(taskClient.findById(task.getId()));
        }
    }

    @After
    public void after() {
        taskClient.clear();
        projectClient.clear();
    }

}
