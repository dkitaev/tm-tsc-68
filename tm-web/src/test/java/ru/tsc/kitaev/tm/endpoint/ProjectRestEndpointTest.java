package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.client.ProjectRestEndpointClient;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.marker.IntegrationCategory;
import ru.tsc.kitaev.tm.model.Project;

import java.util.List;

public class ProjectRestEndpointTest {

    @NotNull
    private final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    private final Project project1 = new Project("Test Project 1", "Test Project Description 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2", "Test Project Description 2");

    @NotNull
    private final Project project3 = new Project("Test Project 3", "Test Project Description 3");

    private int count = 0;

    @Before
    public void before() {
        client.add(project1);
        client.add(project2);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addByProjectTest() {
        @Nullable Project project = client.add(project3);
        Assert.assertNotNull(project);
        Assert.assertEquals(project3.getName(), project.getName());
        Assert.assertEquals(project3.getDescription(), project.getDescription());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveByProjectTest() {
        @Nullable Project project = client.findById(project1.getId());
        project.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(client.save(project));
        @Nullable Project newProject = client.findById(project1.getId());
        Assert.assertEquals(project.getStatus(), newProject.getStatus());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllByProjectsTest() {
        @Nullable final List<Project> projects = client.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(count + 2, projects.size());
        for (@NotNull Project project : projects) {
            Assert.assertNotNull(client.findById(project.getId()));
        }
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdByProjectTest() {
        @Nullable final Project project = client.findById(project1.getId());
        Assert.assertEquals("Test Project 1", project.getName());
        Assert.assertEquals("Test Project Description 1", project.getDescription());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByIdByProjectTest() {
        Assert.assertTrue(client.existsById(project1.getId()));
        Assert.assertFalse(client.existsById(project3.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void getSizeProjects() {
        Assert.assertEquals(count + 2, client.getSize());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clearByProject() {
        Assert.assertEquals(count + 2, client.getSize());
        client.clear();
        Assert.assertEquals(count, client.getSize());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByProjectTest() {
        client.delete(project1);
        Assert.assertNull(client.findById(project1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByProjectIdTest() {
        client.deleteById(project1.getId());
        Assert.assertNull(client.findById(project1.getId()));
    }

    @After
    public void after() {
        client.clear();
    }

}
