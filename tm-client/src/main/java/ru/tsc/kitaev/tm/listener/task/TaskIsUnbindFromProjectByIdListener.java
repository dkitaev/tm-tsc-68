package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskIsUnbindFromProjectByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "tasks-unbind-from-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    @EventListener(condition = "@taskIsUnbindFromProjectByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (projectEndpoint.findProjectById(sessionService.getSession(), projectId) == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        if (taskEndpoint.findTaskById(sessionService.getSession(), taskId) == null) throw new TaskNotFoundException();
        projectTaskEndpoint.unbindTaskById(sessionService.getSession(), projectId, taskId);
    }

}
