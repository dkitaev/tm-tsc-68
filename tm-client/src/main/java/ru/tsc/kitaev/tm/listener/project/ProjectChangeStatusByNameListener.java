package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.endpoint.Status;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByNameListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-change-status-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change status project by name...";
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        projectEndpoint.changeProjectStatusByName(sessionService.getSession(), name, status);
    }

}
