CREATE TABLE IF NOT EXISTS public.projects
(
    id          character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name        character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    user_id     character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status      character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED'::character varying,
    start_date  timestamp(0) without time zone,
    finish_date timestamp(0) without time zone,
    CONSTRAINT projects_pkey PRIMARY KEY (id),
    CONSTRAINT projects_fk FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH FULL
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
    TABLESPACE pg_default;

ALTER TABLE public.projects
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public.sessions
(
    id         character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_id    character varying(255) COLLATE pg_catalog."default" NOT NULL,
    signature  character varying(255) COLLATE pg_catalog."default",
    time_stamp timestamp(0) without time zone,
    CONSTRAINT sessions_pkey PRIMARY KEY (id),
    CONSTRAINT sessions_fk FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
    TABLESPACE pg_default;

ALTER TABLE public.sessions
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public.tasks
(
    id          character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name        character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    user_id     character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status      character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED'::character varying,
    start_date  timestamp(0) without time zone,
    finish_date timestamp(0) without time zone,
    project_id  character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT tasks_pkey PRIMARY KEY (id),
    CONSTRAINT tasks_fk FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH FULL
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT tasks_fk1 FOREIGN KEY (project_id)
        REFERENCES public.projects (id) MATCH FULL
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
    TABLESPACE pg_default;

ALTER TABLE public.tasks
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public.users
(
    id            character varying(255) COLLATE pg_catalog."default" NOT NULL,
    login         character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password_hash character varying(255) COLLATE pg_catalog."default" NOT NULL,
    email         character varying(255) COLLATE pg_catalog."default",
    first_name    character varying(255) COLLATE pg_catalog."default",
    last_name     character varying(255) COLLATE pg_catalog."default",
    middle_name   character varying(255) COLLATE pg_catalog."default",
    role          character varying(255) COLLATE pg_catalog."default" NOT NULL,
    locked        boolean                                             NOT NULL DEFAULT false,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
    TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;